
# Reference: <https://postmarketos.org/devicepkg>
# Maintainer: Alexander Martinz <amartinz@shiftphones.com>

pkgname=device-shift-axolotl
pkgdesc="SHIFT6mq"
pkgver=9
pkgrel=0
url="https://postmarketos.org"
license="MIT"
arch="aarch64"
options="!check !archcheck"
depends="
	linux-postmarketos-qcom-sdm845
	make-dynpart-mappings
	mkbootimg
	postmarketos-base
	postmarketos-update-kernel
	soc-qcom-sdm845
	soc-qcom-sdm845-ucm
	soc-qcom-sdm845-qbootctl
	fwupd>=1.8.6
"
makedepends="devicepkg-dev"
source="
	deviceinfo
	phoc.ini
	q6voiced.conf
"
subpackages="
	$pkgname-nonfree-firmware:nonfree_firmware
	$pkgname-phosh
"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname
}

nonfree_firmware() {
	pkgdesc="Modem, GPU and WiFi Firmware, also needed for osk-sdl"
	depends="firmware-shift-sdm845 firmware-shift-sdm845-initramfs
		 soc-qcom-sdm845-nonfree-firmware soc-qcom-sdm845-modem"
	mkdir "$subpkgdir"

	install -Dm644 q6voiced.conf "$subpkgdir"/etc/conf.d/q6voiced
}

phosh() {
	install_if="$pkgname=$pkgver-r$pkgrel phosh"
	install -Dm644 "$srcdir"/phoc.ini \
		"$subpkgdir"/etc/phosh/phoc.ini
}

sha512sums="
baaa2434b61337b82362c9cbe8e1d26a8d22ccf9aeb0a15f281c0fa241ded9304ff4daf53bc6de10abe7d01ae3613cde95fcba23ab817f6ccd66a66bc30c960f  deviceinfo
65f74d29ab318b23f7ac8923a3661f2c4130049273a8c6be234ed2c8e00810b6a1254b54cf0932e42850a6d48f821dcd038900fbb836feeddfab2759afffae38  phoc.ini
2a3cbdad5aa91181d24a4e56e63986af6accf887c64fa4d1b6ddad1926e29050243c4b1e2fd892d38ca7c16e24dff8545a16552e5749ae31b6d01bf428468e51  q6voiced.conf
"
