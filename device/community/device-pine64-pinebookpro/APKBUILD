# Reference: <https://postmarketos.org/devicepkg>
# Maintainer: Clayton Craft <clayton@craftyguy.net>
pkgname=device-pine64-pinebookpro
pkgdesc="PINE64 Pinebook Pro"
pkgver=16
pkgrel=1
url="https://postmarketos.org"
license="MIT"
arch="aarch64"
options="!check !archcheck"
depends="
	alsa-ucm-conf>=1.2.7
	eudev-hwids
	linux-postmarketos-rockchip>=5.14
	nvme-cli
	postmarketos-base>=25-r1
	u-boot-pinebookpro
	iw
	"
makedepends="devicepkg-dev"
subpackages="$pkgname-nonfree-firmware:nonfree_firmware $pkgname-elogind"
install="
	$pkgname.post-install
	$pkgname.post-upgrade
	$pkgname-elogind.post-install
	$pkgname-elogind.post-upgrade
"
source="
	deviceinfo
	nvme-powersave.initd
	wlan-powersave-off.start
	enable-s2idle.conf
"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname

	install -D -m755 "$srcdir"/nvme-powersave.initd \
		"$pkgdir"/etc/init.d/nvme-powersave

	install -D -m755 "$srcdir"/wlan-powersave-off.start \
		"$pkgdir"/etc/local.d/wlan-powersave-off.start
}

elogind() {
	install_if="$pkgname=$pkgver-r$pkgrel elogind"
	install -D -m644 "$srcdir"/enable-s2idle.conf \
		"$subpkgdir"/usr/lib/elogind/sleep.conf.d/enable-s2idle.conf
}

nonfree_firmware() {
	pkgdesc="Wifi, Bluetooth and video-out firmware"
	depends="linux-firmware-brcm linux-firmware-rockchip"
	mkdir "$subpkgdir"
}
sha512sums="
0933548a542098458c0beab4c90ae51a325b284a6c699015ff1179bef607aa9398171e1abc5e851111e0b097ef848577a41ca6b45f2d8349726563f265b8e8f5  deviceinfo
2abfa31fa56028339efc6d2b6cb838f310990d7110ac4dd996bc4cfcf90f0a8770e70f28f77f7b6929d110e494bc33731302f16b9717729f991c2732be0731d5  nvme-powersave.initd
0fd7580e197e409978c219726af627cabe347404ee826e32892b61f3da1775b0fd81f8ed5e9a0f0bfaad87faa434d7a73ddaebf475ed0ad8fdcef6690cd692d3  wlan-powersave-off.start
811af9c79151f41e25916c0f0bb130e82ebfb7d4d5b63413a3fed6700f829c1c017a12e85da230ed2f47a6bd49ed2bf5da19bbc4576ce485a48a2d3808a3a691  enable-s2idle.conf
"
